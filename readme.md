#Simple Golang Web Server
The purpose of this project is to demonstrate simply how to create a small restful web server that supports the following request types:

 - GET
 - POST
 - DELETE

The project demonstrates how one might use a front-end like a web page to communicate with a backend web service to retrieve and store data.

The project is exemplified by retrieving, storing and deleting colours.

##dependencies
- golang
- github.com/gorilla/mux

##Contents
This repo contains code that is meant to run as a server and code that is meant to be a front-end view which communicates with that server.

###Front-End
The main functional files for the front-end are:

- index.html
- include/script/colours.js `controls the main interaction with DOM`
- include/script/requests.js `performs XMLHTTPRequests.`

The style and image file included are merely to make the demonstration look a little more interesting. They can safely be ignored.

####package.json
Although a package.json was included Node is not a requirement as there are no dependencies.

###Back-End
- main.go is the entry-point for the server.
- route.go handles router setup and functionality.

#Usage
Usage instructions for this example:

##Compile the go server.
1. If you do not have golang installed you may find a binary here: https://golang.org/dl/
    * Go to the provided url and follow their directions for installing and setting up go on your machine
2. open a commandline
3. `cd` into the project's go directory.
4. use `go buid -o GoServer.exe` to build the server
    * if you get build errors you may need to run `go get github.com/gorilla/mux` to retrieve the project dependency.
5. run the server: `./GoServer.exe`

**NB:** A new file will be generated in the working directory of the GoServer (default name colors.txt). This file functions as the backing storage for the colors available to the server. All colors added on the server will be written to this file.

##Open index.html
Once the server is compiled and running you are ready to go.

1. With the server running, open the index.html file.
2. Use the form inputs to interact with the server.