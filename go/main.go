package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

//Slice storing colors in memory.
var colors []string

const colorDataStore = "colors.txt"
const defaultColors = "#FFCC00:#F5F5F5:#FF7F50:#8B4513:#1E90FF:#CD853F:#E6E6FA:#90EE90:#DB7093"
const port = ":7150"

//Entry-Point
func main() {
	EnsureColorStore()
	LoadColorsToMemory()

	//setup and run routes
	SetupRoutes()
	ServeAPI()
}

//EnsureColorStore checks for the existence of a colour store to load/save colors.
func EnsureColorStore() {
	if _, err := os.Stat(colorDataStore); os.IsNotExist(err) {
		fmt.Println("No color file found. Creating one now")
		CreateColorFile()
	}
}

//CreateColorFile creates a colors.txt
func CreateColorFile() {
	ioutil.WriteFile(colorDataStore, []byte(defaultColors), 0644)
}

//PersistColors is a helper method that gets called when a new color is added or removed to persist the new state of the color collection
func PersistColors() {
	w := strings.Join(colors, ":")
	ioutil.WriteFile("colors.txt", []byte(w), 0644)
}

//LoadColorsToMemory reads the colorDataStore and puts the colors into memory.
func LoadColorsToMemory() {
	b, _ := ioutil.ReadFile(colorDataStore)
	values := strings.Split(string(b), ":")
	for _, element := range values {
		colors = append(colors, element)
	}
}
