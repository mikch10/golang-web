package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

var router *mux.Router

//SetupRoutes sets up the routes for the HTTP-Server
func SetupRoutes() {
	router = mux.NewRouter()
	router.HandleFunc("/color/list", GetColorList).Methods("GET")
	router.HandleFunc("/color/get/{index}", GetColor).Methods("GET")
	router.HandleFunc("/color/random", GetRandomColor).Methods("GET")
	router.HandleFunc("/color/{hex}", CreateColor).Methods("POST")
	router.HandleFunc("/color/{hex}", DeleteColor).Methods("DELETE")
	router.HandleFunc("/color/{hex}", indexHandler).Methods("OPTIONS")
}

//ServeAPI Start the server and listen on port.
func ServeAPI() {
	log.Fatal(http.ListenAndServe(port, router))
}

/**
 * Route functions
**/

/***************************
 * 			GET 		   *
****************************/

//GetColorList get a string with all colours
func GetColorList(w http.ResponseWriter, r *http.Request) {
	EnableCors(&w)
	fmt.Println("Get request received for GetColors...")
	json.NewEncoder(w).Encode(colors)
}

//GetRandomColor gets a random color
func GetRandomColor(w http.ResponseWriter, r *http.Request) {
	EnableCors(&w)
	rand.Seed(time.Now().UTC().UnixNano())

	c := colors[rand.Intn(len(colors))]
	fmt.Println("returning random color ", c)
	json.NewEncoder(w).Encode(c)
}

//GetColor Returns a colour from specified index.
func GetColor(w http.ResponseWriter, r *http.Request) {
	EnableCors(&w)
	parameters := mux.Vars(r)
	index, err := strconv.Atoi(parameters["index"])
	if err != nil {
		json.NewEncoder(w).Encode("Parameter must be int.")
		return
	}
	if index < 0 {
		index = 0
	}
	if index > len(colors) {
		json.NewEncoder(w).Encode(colors[len(colors)-1])
		return
	}
	json.NewEncoder(w).Encode(colors[index])
}

/***************************
 * 			POST 		   *
****************************/

//CreateColor add a new color
func CreateColor(w http.ResponseWriter, r *http.Request) {
	EnableCors(&w)
	parameters := mux.Vars(r)
	color := "#" + parameters["hex"]
	for _, c := range colors {
		if color == c {
			fmt.Println("color already exists. Discarding")
			json.NewEncoder(w).Encode("Color Already Exists. Discarded")
			return
		}
	}
	fmt.Println("received new color ", color)
	json.NewEncoder(w).Encode("Persisted new color")
	colors = append(colors, color)
	PersistColors()
}

/***************************
 * 			DELETE 		   *
****************************/

//DeleteColor delete a known color
func DeleteColor(w http.ResponseWriter, r *http.Request) {
	EnableCors(&w)
	parameters := mux.Vars(r)
	color := "#" + parameters["hex"]
	didDelete := false
	for i, c := range colors {
		if color == c {
			colors = append(colors[:i], colors[i+1:]...)
			didDelete = true
			break
		}
	}
	if !didDelete {
		fmt.Println("Did not find color. Nothing was deleted")
		json.NewEncoder(w).Encode("Did not find color. Nothing was deleted")
		return
	}
	fmt.Println("Deleted color ", color)
	json.NewEncoder(w).Encode("Color was deleted")
	PersistColors()
}

/***************************
 * 			OPTIONS		   *
****************************/
func indexHandler(w http.ResponseWriter, req *http.Request) {
	SetupResponse(&w, req)
	if (*req).Method == "OPTIONS" {
		return
	}
}

/***************************
 * 			Util 		   *
****************************/

//EnableCors call to enable Cross-Origin for a particular route
func EnableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

//SetupResponse used to handle pre-flight on OPTIONS requests.
func SetupResponse(w *http.ResponseWriter, req *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}
