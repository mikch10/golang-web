

//list of colors for mocking web service functionality.
function changeColour(targetId) {
    let target = document.getElementById(targetId);
    let color = getColor();
    target.style.backgroundColor = color;
    target.setAttribute("data-current-color", color);
    let colorAsRGB = toRGB(color);
    let brightness = brightnessFromRGB(colorAsRGB);
    if (brightness > 150)
        setAfterFont("dark");
    else
        setAfterFont("");
}

function toRGB(hex) {
    hex = hex.replace("#", "");
    let result = {
        "r": parseInt(hex.substring(0, 2), 16),
        "g": parseInt(hex.substring(2, 4), 16),
        "b": parseInt(hex.substring(4, 6), 16)
    }
    return result;
}

function brightnessFromRGB(color) {
    return (color.r * 299 + color.g * 587 + color.b * 114) / 1000
}

function setAfterFont(action) {
    let target = document.getElementById("target");
    switch (action) {
        case "dark":
            target.classList.add("dark-label");
            break;
        default:
            target.classList.remove("dark-label");
            break;
    }
}


function getColor() {
    let color = getRandomColor();
    color = color.replace(new RegExp(/"/, 'g'), "");
    return color;
}


function saveColor(elementId) {
    let color = validateAndGetColor(elementId);
    if (color === "") {
        alert("colors must be hexadecimal format");
        return;
    }
    console.log(addColor(color));
}

function validateAndGetColor(elementId) {
    let validator = /^[0-9A-F]{6}$/i;
    let element = document.getElementById(elementId);
    let color = element.value;
    element.value = "";
    if (validator.test(color))
        return color;
    return "";
}

//todo: Replace mock-code with web-service ajax call.
function deleteColor(elementId) {
    let color = validateAndGetColor(elementId);
    if (color === "") {
        alert("colors must be in a hexidecimal format")
        return;
    }
    console.log(removeColor(color));
}

function getColorList() {
    let element = document.getElementById("list-container");
    let colors = JSON.parse(getAllColors());
    element.innerHTML = "";
    colors.forEach(c => {
        element.innerHTML += c + " - ";
    });
    let str = element.innerHTML
    element.innerHTML = str.substring(0, str.length - 2);

}