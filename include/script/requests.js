const baseUrl = "http://localhost:7150/"
const allColorsEndpoint = baseUrl + "color/list";
const specificColorEndpoint = baseUrl + "color/get/";
const randomColorEndpoint = baseUrl + "color/random";
const postDeleteEndpoint = baseUrl + "color/"

function getRandomColor() {
    var request = new XMLHttpRequest();
    request.open("GET", randomColorEndpoint, false);
    request.send();
    return request.responseText;
}

function getAllColors() {
    var request = new XMLHttpRequest();
    request.open("GET", allColorsEndpoint, false);
    request.send();
    return request.responseText;
}

function addColor(color) {
    var request = new XMLHttpRequest();
    request.open("POST", postDeleteEndpoint + color, false);
    request.send();
    return request.responseText;
}

function removeColor(color) {
    var request = new XMLHttpRequest();
    request.open("DELETE", postDeleteEndpoint + color, false);
    request.send();
    return request.responseText;
}